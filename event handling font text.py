from OpenGLContext import testingcontext
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


# Koordinat x dan y untuk posisi kotak
pos_x = 0
pos_y = 0

# Warna Kotak
merah = 0
hijau = 0
biru = 0

# Warna Teks
teks_merah = 1
teks_hijau = 1
teks_biru = 1

# Warna Objek
warna_objek = "Hitam"

# Warna Background
warna_background = "Hitam"

def init():
    glClearColor(0.0, 0.0, 0.0, 1.0)
    gluOrtho2D(-500.0, 500.0, -500.0, 500.0)

def drawBitmapText(string,x,y,z) :
    glRasterPos3f(x,y,z)
    for c in string :
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,ord(c))

def reshape(w, h):
    glViewport(0,0,w,h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0,w,h,0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def Text():
    glColor3f(teks_merah,teks_hijau,teks_biru)
    drawBitmapText("Warna",-460,-350,0)
    drawBitmapText("Objek : " + warna_objek ,-460,-400,0)
    drawBitmapText("Backgorund : " + warna_background ,-460,-450,0)
    
#membuat objek kotak
def objek_kotak():
    global pos_x, pos_y
    glColor3f(merah,hijau,biru)    
    glBegin(GL_POLYGON)
    # kiri bawah
    glVertex2f(-70 + pos_x, -70 + pos_y)
    # kanan bawah
    glVertex2f(70 + pos_x, -70 + pos_y)
    # Kanan atas
    glVertex2f(70 + pos_x, 70 + pos_y)
    # kanan Bawah
    glVertex2f(-70 + pos_x, 70 + pos_y)
    glEnd()

#membuat garis pembatas kartesius x,y
def tampil():
    glClear(GL_COLOR_BUFFER_BIT)
    Text();
    glColor3f(1.0,1.0,1.0)
    #mengunakan objek garis
    glBegin(GL_LINES)
    glVertex2f(-500.0, 0.0)
    glVertex2f(500.0, 0.0)
    glVertex2f(0.0, 500.0)
    glVertex2f(0.0, -500.0)
    glEnd()
    objek_kotak()
    glFlush()

#mouse klik
def mouse(button, state, x, y):
    global merah, hijau, biru
    global warna_objek
    if button == GLUT_RIGHT_BUTTON and state == GLUT_DOWN:
        if hijau < 1:
            merah = 0
            hijau = 1
            biru = 0
            warna_objek = "Hijau"
        elif biru < 1:
            merah = 0
            hijau = 0
            biru = 1
            warna_objek = "Biru"
        print("mouse kanan di klik ", "(", x, ",", y, ")")
    elif button == GLUT_LEFT_BUTTON and state == GLUT_DOWN:
        if merah < 1:
            merah = 1
            hijau = 0
            biru = 0
            warna_objek = "Merah"
        else:
            merah = 0
            hijau = 0
            biru = 0
            warna_objek = "Hitam"
        print("mouse kiri di klik ", "(", x, ",", y, ")")

#menggerakkan kotak dengan keyboard
def keyboard(key,x,y):
    global pos_x, pos_y
    global warna_background
    global teks_merah, teks_hijau, teks_biru

    #Mengubah posisi kotak, perpindahan senilai 5

    if key == GLUT_KEY_UP:
        pos_y += 5
        print("Tombol Atas di klik ", "x : ", pos_x, " y : ", pos_y)
    elif key == GLUT_KEY_DOWN:
        pos_y -= 5
        print("Tombol Bawah di klik ", "x : ", pos_x, " y : ", pos_y)
    elif key == GLUT_KEY_RIGHT:
        pos_x += 5
        print("Tombol Kanan di klik ", "x : ", pos_x, " y : ", pos_y)
    elif key == GLUT_KEY_LEFT:
        pos_x -= 5
        print("Tombol Kiri di klik ", "x : ", pos_x, " y : ", pos_y)

    #Mengubah Warna backgorund window
    #Kiri Atas (Merah)
    if pos_x < 0 and pos_y > 0:
        glClearColor(1.0, 0.0, 0.0, 1.0)
        warna_background = "Merah"
        teks_merah = 0
        teks_hijau = 0
        teks_biru = 0
    #Kiri Bawah (Hitam)
    if pos_x < 0 and pos_y < 0:
        glClearColor(0.0,0.0,0.0,1.0)
        warna_background = "Hitam"
        teks_merah = 1
        teks_hijau = 1
        teks_biru = 1
    #Kanan Atas (Hijau)
    if pos_x > 0 and pos_y > 0:
        glClearColor(0.0, 1.0, 0.0, 1.0)
        warna_background = "Hijau"
        teks_merah = 0
        teks_hijau = 0
        teks_biru = 0
    #Kanan Bawah (Biru)
    if pos_x > 0 and pos_y < 0:
        glClearColor(0.0,0.0,1.0,1.0)
        warna_background = "Biru"
        teks_merah = 1
        teks_hijau = 1
        teks_biru = 1
    
def update(value):
    glutPostRedisplay()
    glutTimerFunc(10,update,0)

def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB)
    #set ukuran window
    glutInitWindowSize(500,500) 
    #posisi window
    glutInitWindowPosition(100,100)
    #Nama window
    glutCreateWindow("Tugas Pertemuan 9 (17102056)")
    #menjalankan funtion tampil
    glutDisplayFunc(tampil)
    #menjalankan funtion keyboard
    glutSpecialFunc(keyboard)
    #menjalankan funtion mouse
    glutMouseFunc(mouse)
    glutTimerFunc(50, update, 0)

    init()
    glutMainLoop()
    
main()
